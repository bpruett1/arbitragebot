﻿using System;
using System.Threading.Tasks;
using Nethereum.Web3;

namespace Arbirage_bot
{
    class Program
    {
        static void Main(string[] args)
        {
            GetAccountBalance().Wait();
            Console.ReadLine();
        }

        static async Task GetAccountBalance()
        {
            var web3 = new Web3("https://mainnet.infura.io/v3/API"); // Infura API
            var balance = await web3.Eth.GetBalance.SendRequestAsync("Account"); // Wallet Account 
            var etherAmount = Web3.Convert.FromWei(balance.Value);
            Console.WriteLine($"Balance in Ether: {etherAmount}");
        }
    }
}